//
//  RecipeCell.swift
//  MisRecetas
//
//  Created by Jesus Navarrete Perez on 25/2/17.
//  Copyright © 2017 dsmonkey. All rights reserved.
//

import UIKit

class RecipeCell : UITableViewCell{
    
    @IBOutlet var ThumbnailImageView: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var ingredientsLabel: UILabel!
    
}
