//
//  ViewController.swift
//  MisRecetas
//
//  Created by Jesus Navarrete Perez on 22/2/17.
//  Copyright © 2017 dsmonkey. All rights reserved.............
//

import UIKit

class ViewController: UITableViewController {
    
    
    var recipes : [Recipe] = []

    override func viewDidLoad() {
        super.viewDidLoad()
      
        var recipe = Recipe(name: "Tortilla de patatas",
                            image: #imageLiteral(resourceName: "tortilla"),
                            time: 35,
                            ingredients: ["Patatas","Cebolla","Huevos"],
                            steps: ["1.Freir las patatas","2.Pochar la cebolla", "3.Batir los huevos","4.Mezclar todo y cuajar los huevos"])
        
        recipes.append(recipe)
        
        recipe = Recipe(name: "Pizza Margarita", image: #imageLiteral(resourceName: "pizza"),
                        time: 25,
                        ingredients: ["Masa pizza","Tomate","Queso","Jamon cocido"],
                        steps: ["1.Estender la masa","2.Estender el tomate","3.Esparcir el queso","4.Añadir el jamos cocido","5.Hornear 15 minutos a 180 grados"])
        
        recipes.append(recipe)
        
        recipe = Recipe(name: "Hamburguesa con queso", image : #imageLiteral(resourceName: "hamburger2"),
                        time: 15,
                        ingredients: ["Pan de hamburguesa","Cebolla","Carne picada","Queso","Albahaca freca"],
                        steps: ["1.Asar la carne a la parrilla","2.Fundir el queso sobre la hamburguesa", "3.Pochar la cebolla y cubrir el queso","4.Añadir uns hojas de albahaca"])
        
        recipes.append(recipe)
        
        recipe = Recipe(name: "Ensada César", image : #imageLiteral(resourceName: "ensañada"),
                        time: 15,
                        ingredients: ["Vegetales","Pollo","Salsa Cesar"],
                        steps: ["1.Mezclar los vegetales","2.Freir el pollo y mezclar con los vejetales","3.Añadir la salsa cesar"])
        
        recipes.append(recipe)
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override var prefersStatusBarHidden: Bool{ // funcion que esconde la barra sumerior del dispositivo
        return true
    }
    
    
    
    
    
    
    
    //MARK: - UITableViewDataSource  - Estas tres funciones identifican las secciones, Las filas, y la celda
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recipes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let recipe = recipes[indexPath.row]
        let cellID = "RecipeCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
    
        cell.ThumbnailImageView.image = recipe.image
        cell.nameLabel.text = recipe.name
        cell.timeLabel.text = "\(recipe.time!) Mimutos"
        cell.ingredientsLabel.text = "Ingredientes: \(recipe.ingredients.count)"
        
        
        
        if recipe.isFavourite{
            cell.accessoryType = .checkmark
        }else {
            cell.accessoryType = .none
        }
        
        cell.ThumbnailImageView.layer.borderWidth = 3
        cell.ThumbnailImageView.layer.borderColor = (UIColor.lightGray).cgColor
        cell.ThumbnailImageView.layer.shadowColor = UIColor.black.cgColor
        //cell.ThumbnailImageView.layer.shadowOffset = CGSize(width: 10, height: 5)
        //cell.ThumbnailImageView.layer.shadowOpacity = 0.5
        
        return cell
    }
    
   // Funciones sobreecritas para eliminar y compartir al deslizar
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) { //Para eliminar
        if editingStyle == .delete {
            self.recipes.remove(at: indexPath.row)
            
        }
        self.tableView.deleteRows(at: [indexPath], with: .fade)
    
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        //Compartir
        
        let shareAction = UITableViewRowAction (style: .default, title: "Compartir") { (action, indexPath) in
            
            let sharedDefaulText = "Estoy mirando la receta de \(self.recipes[indexPath.row].name!) en una aplicación muy chula de iOS 10"
            
            let activityController = UIActivityViewController(activityItems: [sharedDefaulText, self.recipes[indexPath.row].image!], applicationActivities: nil)
            self.present (activityController, animated: true, completion: nil)
        
        }
        
        
        shareAction.backgroundColor = UIColor(colorLiteralRed: 30.0/255.0, green: 164.0/255.0, blue: 253.0/255.0, alpha: 1.0) // color del compartir
        
        //Borrar
        
        let deleteAction = UITableViewRowAction (style: .default, title: "Borrar") { (action, indexPath) in
            self.recipes.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)

        }
        
           deleteAction.backgroundColor = UIColor(colorLiteralRed: 202.0/255.0, green: 202.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        
return [shareAction, deleteAction]
    }
    
    
    
    // MARK - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let recipe = recipes[indexPath.row]
        
        let alertComtroller = UIAlertController(title: recipe.name, message: "Valora el plato", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alertComtroller.addAction(cancelAction)
        
        var favouriteActionTitle = "Favorito"
        var favouriteActionStyle = UIAlertActionStyle.default
        if recipe.isFavourite {
            favouriteActionTitle = "No favorito"
            favouriteActionStyle = UIAlertActionStyle.destructive
        }
        
        let favouriteAction = UIAlertAction(title: favouriteActionTitle, style: favouriteActionStyle) { (action) in
            let recipe = self.recipes[indexPath.row]
            recipe.isFavourite = !recipe.isFavourite
            self.tableView.reloadData()
        }
         alertComtroller.addAction(favouriteAction)
        self.present(alertComtroller, animated: true, completion: nil)
        
        
    }
    
}

